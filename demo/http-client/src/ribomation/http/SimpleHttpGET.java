package ribomation.http;
import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import java.net.URI;
import java.util.stream.Stream;

public class SimpleHttpGET {
    public static void main(String[] args) throws Exception {
        HttpRequest req = HttpRequest.newBuilder()
                .uri(URI.create("https://www.google.se/robots.txt"))
                .header("Accept", "text/plain")
                .GET()
                .build();

        HttpClient client = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .build();

        HttpResponse<String> res = client
                .send(req, HttpResponse.BodyHandler.asString());

        System.out.printf("Status: %d%n", res.statusCode());
        System.out.println("------");
        Stream.of(res.body().split("\\n"))
                .limit(6)
                .forEach(System.out::println);
    }
}

