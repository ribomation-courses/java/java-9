package ribomation.jshell_usage;
import jdk.jshell.JShell;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class UsingJShell {
    public static void main(String[] args) {
        UsingJShell app = new UsingJShell();
        app.run(10);
        app.run(100);
        app.run(1_000);
        app.run(10_000);
    }

    private void run(int n) {
        List<String> snippets = Arrays.asList(
                String.format("int n = %d;", n),
                "long sum(int n) {return n*(n+1)/2;}",
                "System.out.printf(\"SUM(%d) = %d%n\", n, sum(n));"
        );

        ByteArrayOutputStream buf   = new ByteArrayOutputStream();
        JShell                shell = JShell.builder().out(new PrintStream(buf)).build();
        snippets.forEach(shell::eval);
        System.out.printf("RESULT: %s", buf.toString());
    }
}


