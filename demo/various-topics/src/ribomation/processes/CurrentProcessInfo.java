package ribomation.processes;

import java.time.Duration;
import java.util.Arrays;

public class CurrentProcessInfo {
    public static void main(String[] args) {
        ProcessHandle current = ProcessHandle.current();

        System.out.printf(" PID    : %d%n"   , current.pid());
        System.out.printf("PPID    : %d%n"   , current.parent().orElse(current).pid());

        ProcessHandle.Info info = current.info();
        System.out.printf("Username: %s%n"   , info.user().orElse("-"));
        System.out.printf("Elapsed : %.5fs%n", info.totalCpuDuration().orElse(Duration.ZERO).toNanos() * 1E-9);
        System.out.printf("Exe     : %s%n"   , info.command().orElse("-"));
        System.out.printf("Cmd Line: %s%n"   , info.commandLine().orElse("-"));
        System.out.printf("Cmd Args: %s%n"   , Arrays.toString(info.arguments().orElse(new String[]{"-"})));
        System.out.printf("Pgm Args: %s%n"   , Arrays.toString(args));

        System.out.printf("Country : %s%n"   , System.getProperty("user.country"));
    }
}


