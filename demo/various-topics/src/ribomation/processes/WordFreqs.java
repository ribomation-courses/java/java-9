package ribomation.processes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class WordFreqs {
    public static void main(String[] args) throws IOException {
        String clsname = WordFreqs.class.getName();
        String file    = String.format("./src/%s.java", clsname.replace('.', '/'));
        System.out.printf("file: %s%n", file);

        List<Process> pipeline = ProcessBuilder.startPipeline(List.of(
                new ProcessBuilder("cat", file),
                new ProcessBuilder("tr", " ,:;.*=%\\-\"{}()[]", "\\n"),
                new ProcessBuilder("egrep", "\\w+"),
                new ProcessBuilder("sort"),
                new ProcessBuilder("uniq", "-c"),
                new ProcessBuilder("sort", "-n", "-r", "-k", "1"),
                new ProcessBuilder("head", "-12")
        ));

        Process last = pipeline.get(pipeline.size() - 1);
        if (last.toHandle().isAlive()) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            last.getInputStream()));
            try (in) {
                in.lines().forEach(System.out::println);
            }
        }
    }
}


