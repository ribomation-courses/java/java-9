package ribomation.stack_trace;

public class UsingStackWalker {
    public static void main(String[] args) {
        new A().doit(new B(), new C());
    }

    static class A {
        void doit(B b, C c) {
            System.out.println("A.doit()");
            b.perform(c);
        }
    }

    static class B {
        void perform(C c) {
            System.out.println("B.perform()");
            c.work();
        }
    }

    static class C {
        void work() {
            System.out.println("C.work()");
            walk();
        }
    }

    static void walk() {
        System.out.println("----");
        StackWalker.getInstance().walk(frames -> {
            frames.limit(10)
                    .map(StackWalker.StackFrame::getClassName)
                    .distinct()
                    .forEach(System.out::println);
            return null;
        });
    }

}


