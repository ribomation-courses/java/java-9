package ribomation.deprecated_usage;

import java.util.Calendar;
import java.util.Date;

public class UsingDeprecatedMethods {
    public static void main(String[] args) {
        Date date = new Date(2018 - 1900, Calendar.MARCH, 23);
        System.out.printf("date: %s%n", date);
    }
}


