package ribomation.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class SequenceJava9 {
    interface Sequence<T> {
        T       first();
        T       next();
        boolean notLast();

        default List<T> all() {
            return traverse((e, lst) -> lst.add(e));
        }

        default List<T> apply(UnaryOperator<T> f) {
            return traverse((e, lst) -> lst.add(f.apply(e)));
        }

        default List<T> filter(Predicate<T> p) {
            return traverse((e, lst) -> { if (p.test(e)) lst.add(e); });
        }

        private List<T> traverse(BiConsumer<T, List<T>> doit) {
            List<T> lst = new ArrayList<>();
            for (T elem = first(); notLast(); elem = next()) doit.accept(elem, lst);
            return lst;
        }
    }

    static class IntSequence implements Sequence<Integer> {
        private final int lower;
        private final int upper;
        private       int current;

        @Override
        public Integer first() {
            return current = lower;
        }

        @Override
        public boolean notLast() {
            return current <= upper;
        }

        @Override
        public Integer next() {
            return ++current;
        }

        public IntSequence(int lower, int upper) {
            this.lower = lower;
            this.upper = upper;
        }
    }

    public static void main(String[] args) {
        IntSequence s = new IntSequence(1, 10);
        System.out.printf("ALL   : %s%n", s.all());
        System.out.printf("APPLY : %s%n", s.apply(n -> n * n));
        System.out.printf("FILTER: %s%n", s.filter(n -> n % 2 == 0));
    }

}

