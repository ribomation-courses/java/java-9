package ribomation.interfaces;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class SequenceJava7 {
    interface Sequence<T> {
        T first();
        T next();
        boolean notLast();
    }

    static abstract class AbstractSequence<T> implements Sequence<T> {
        public List<T> all() {
            List<T> lst = new ArrayList<>();
            for (T elem = first(); notLast(); elem = next()) lst.add(elem);
            return lst;
        }

        public List<T> apply(UnaryOperator<T> f) {
            List<T> lst = new ArrayList<>();
            for (T elem = first(); notLast(); elem = next()) lst.add(f.apply(elem));
            return lst;
        }

        public List<T> filter(Predicate<T> p) {
            List<T> lst = new ArrayList<>();
            for (T elem = first(); notLast(); elem = next()) if (p.test(elem)) lst.add(elem);
            return lst;
        }
    }

    static class IntSequence extends AbstractSequence<Integer> {
        private final int lower;
        private final int upper;
        private       int current;

        @Override
        public Integer first() {
            return current = lower;
        }

        @Override
        public boolean notLast() {
            return current <= upper;
        }

        @Override
        public Integer next() {
            return ++current;
        }

        public IntSequence(int lower, int upper) {
            this.lower = lower;
            this.upper = upper;
        }
    }

    public static void main(String[] args) {
        IntSequence s = new IntSequence(1, 10);
        System.out.printf("ALL   : %s%n", s.all());
        System.out.printf("APPLY : %s%n", s.apply(n -> n * n));
        System.out.printf("FILTER: %s%n", s.filter(n -> n % 2 == 0));
    }

}

