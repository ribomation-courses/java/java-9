package ribomation.logger;

import static java.lang.System.Logger.Level.INFO;
import static java.lang.System.Logger.Level.WARNING;

public class UsingSystemLogger {
    public static void main(String[] args) {
        System.Logger log = System.getLogger("dummy");
        log.log(INFO, "hello from a logger");
        log.log(WARNING, "this is a friendly warning");
    }
}


