#!/usr/bin/env bash
set -eux

rm -rf ./out/ribomation-app.jre

jlink --module-path $JAVA_HOME/jmods:./build/jars \
      --add-modules ribomation.app \
      --output ./out/ribomation-app.jre \
      --launcher run=ribomation.app/se.ribomation.app.App

set +x
echo '--------------------'
tree -L 2 ./out/ribomation-app.jre

echo '--------------------'
./out/ribomation-app.jre/bin/run

