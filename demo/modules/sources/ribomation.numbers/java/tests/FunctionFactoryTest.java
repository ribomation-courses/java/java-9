package tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import se.ribomation.numbers.FunctionFactory;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("FunctionFactory test suite")
@SuppressWarnings("ConstantConditions")
public class FunctionFactoryTest {
    FunctionFactory factory;

    @BeforeEach
    public void init() {
        factory = FunctionFactory.instance;
    }

    @Test
    @DisplayName("factory should find all functions")
    public void should_lookup_all_functions() {
        assertNotNull(FunctionFactory.instance);
        assertNotNull(factory);
        assertNotNull(factory.functionNames());
        factory.functionNames()
                .forEach(name ->
                        assertTrue(FunctionFactory.instance.get(name).isPresent())
                );
    }

    @Test
    @DisplayName("factory should return empty options for absent function")
    public void nonexisting_function_should_return_empty_option() {
        assertFalse(factory.get("whatever").isPresent());
    }

    @Test
    @DisplayName("sum(10) should return 55")
    public void sum_10_should_be_55() {
        assertEquals(55, factory.get("sum").get().eval(10));
    }

    @Test
    @DisplayName("fac(5) should return 120")
    public void fac_5_should_be_120() {
        assertEquals(120, factory.get("fac").get().eval(5));
    }

    @Test
    @DisplayName("fib(10) should return 55")
    public void fib_10_should_be_55() {
        assertEquals(55, factory.get("fib").get().eval(10));
    }
}

