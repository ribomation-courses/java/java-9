#!/usr/bin/env bash
set -eux

rm -rf ./out
mkdir -p ./out/classes/ribomation.{numbers, app} ./out/jars

javac --class-path $(ls ./libs/*.jar | tr '\n' ':') \
      --module-path ./libs \
      -d ./out/classes/ribomation.numbers \
      $(find ./sources/ribomation.numbers -name '*.java')

javac --module-path ./out/classes \
      -d ./out/classes/ribomation.app \
      $(find ./sources/ribomation.app -name '*.java')

jar --create --file ./out/jars/ribomation.numbers.jar \
    -C ./out/classes/ribomation.numbers .

jar --create --file ./out/jars/ribomation.app.jar \
    --main-class se.ribomation.app.App \
    -C ./out/classes/ribomation.app .

(set +x ; echo '---- BUILT ----------------')
tree ./out
jar --list --file ./out/jars/ribomation.numbers.jar
jar --list --file ./out/jars/ribomation.app.jar

(set +x ; echo '---- EXECUTION ----------------')
java --module-path ./build/jars --module ribomation.app

