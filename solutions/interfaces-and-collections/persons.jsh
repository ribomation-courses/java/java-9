class Person {
  private String  name;
  private int     age;
  public Person(String name, int age) {this.name=name; this.age=age;}
  public String toString() {return String.format("Person{%s, %d}", name, age);}
  public String getName()  {return name;}
  public int    getAge()   {return age;}
}

List<Person> persons = List.of(
  new Person("Anna", 10),
  new Person("Bertil", 20),
  new Person("Carin", 25),
  new Person("David", 30),
  new Person("Eva", 35),
  new Person("Fredrik", 42),
  new Person("Gun", 46),
  new Person("Hans", 50)
);

persons.stream().
  takeWhile(p -> p.getAge() <= 45).
  forEach(System.out::println);
