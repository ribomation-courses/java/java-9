Java 9, 1 day
====
Welcome to this course.

Here you will find
* Installation instructions
* Source code to demo projects
* Solutions to the programming exercises

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation

    git clone https://gitlab.com/ribomation-courses/java/java-9.git
    cd java-9

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

Java 9
----
In order to do the programming exercises of the course, you need to have
Java 9 JDK installed.
* [Java 9 JDK Download](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

SDKMAN
----
Using [SDKMAN](http://sdkman.io/) is the preferred way of installing and handling Java tools, because you can easily swap versions.

If you already have GIT for Windows and its GIT-BASH window (or running Linux or Mac); you can install SDKMAN by the following command inside a BASH window:

    curl -s "https://get.sdkman.io" | bash
N.B. SDKMAN is not supported for Windows command console! You must use a *NIX like shell.


Using the `sdk` command you can then install Java JDK and other Java tools

    sdk install java 9.0.4-oracle

This how you install Java 8

    sdk install java 8u161-oracle
    use java 8u161-oracle
Then reverting back to Java 9

    use java 9.0.4-oracle
You can list all tools provided by SDKMAN as

    sdk list

Or, all versions of a specific tool

    sdk list java

Read more about SDKMAN at
* http://sdkman.io/
* http://sdkman.io/install.html
* http://sdkman.io/usage.html
* http://sdkman.io/sdks.html

IDE
----
In addition, you need to be using a decent Java IDE or text editor, such as
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download) (_This is my choice and also used during the course_)
* [Eclipse IDE](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/photonm3)
* [NetBeans IDE](https://netbeans.org/downloads/)
* [MS Visual Code](https://code.visualstudio.com/download)
* [ATOM](https://atom.io/)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
